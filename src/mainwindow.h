#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWebEngineView>
#include <QtWebChannel>

#include "qsslpage.h"
#include "inspector.h"
#include "jscontext.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    QWebEngineView * m_webView = nullptr;
    QWebChannel * m_webChannel = nullptr;
    Inspector * m_inspector = nullptr;
    JsContext * m_jsContext = nullptr;

public:
    explicit MainWindow(QWidget *parent = nullptr);

    virtual ~MainWindow();

    MainWindow * operator &();
    MainWindow const * operator &() const;

private:
    MainWindow(const MainWindow &) = delete;
    MainWindow(MainWindow &&) = delete;

    MainWindow & operator =(const MainWindow &) = delete;
    MainWindow & operator =(MainWindow &&) = delete;

private:
    Ui::MainWindow * ui;
};

#endif // MAINWINDOW_H
