#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    qputenv("QTWEBENGINE_REMOTE_DEBUGGING", "7777");

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
