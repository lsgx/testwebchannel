TEMPLATE = app

QT_VER = $$[QT_VERSION]
QT_PATH = $$[QT_INSTALL_PREFIX]

#message(Qt version is $$QT_VER)
#message(Qt install prefix is $$QT_PATH)

QT       += core gui webenginewidgets webchannel

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT       += testlib

CONFIG += debug_and_release

TARGET = samp16_4webchannel
CONFIG(debug, debug|release) {
    unix: TARGET = $$join(TARGET,,,_debug)
    else: TARGET = $$join(TARGET,,,d)
}

VER_MAJ = 0
VER_MIN = 1
VER_PAT = 1
VERSION = $$sprintf("%1.%2.%3",$$VER_MAJ,$$VER_MIN,$$VER_PAT)

# BUILD_DIR = $$_PRO_FILE_PWD_/../build
debug: DESTDIR = debug
release: DESTDIR = release

QMAKE_LIBDIR += $$DESTDIR

UI_DIR += .ui
OBJECTS_DIR += .obj
MOC_DIR += .moc
RCC_DIR += .res


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


#
#下面这些选项控制着使用哪些编译器标志：
# release - 应用程序将以release模式连编。如果“debug”被指定，它将被忽略。
# debug - 应用程序将以debug模式连编。
# warn_on - 编译器会输出尽可能多的警告信息。如果“warn_off”被指定，它将被忽略。
# warn_off - 编译器会输出尽可能少的警告信息。
#
CONFIG(debug, debug|release) {
    CONFIG += debug
    CONFIG += warn_on

    DEFINES += QT_DEBUG
} else {
    CONFIG += release
    CONFIG += warn_off

    DEFINES += QT_NO_DEBUG
    DEFINES += QT_NO_DEBUG_OUTPUT
    DEFINES += QT_NO_WARNING_OUTPUT
}

CONFIG += c++11

#
# 下面这些选项定义了所要连编的库/应用程序的类型
# qt - 应用程序是一个Qt应用程序，并且Qt库将会被连接。
# thread - 应用程序是一个多线程应用程序。
# x11 - 应用程序是一个X11应用程序或库。
# windows - 只用于“app”模板：应用程序是一个Windows下的窗口应用程序。
# console - 只用于“app”模板：应用程序是一个Windows下的控制台应用程序。
# app_bundle - 对于 Mac 上的控制台程序，创建对应的 app bundle , 用户不能与标准I/O交互
# dll - 只用于“lib”模板：库是一个共享库（dll）。
# staticlib - 只用于“lib”模板：库是一个静态库。
# plugin - 只用于“lib”模板：库是一个插件，这将会使dll选项生效。
#
#win32: CONFIG += console
#CONFIG -= app_bundle
#CONFIG -= qt

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

DEFINES += QT_NO_CAST_FROM_ASCII
DEFINES += QT_NO_CAST_TO_ASCII
DEFINES += QT_NO_CAST_FROM_BYTEARRAY
DEFINES += QT_NO_URL_CAST_FROM_STRING
DEFINES += QT_USE_QSTRINGBUILDER

# Disable warning C4819 for msvc
msvc:QMAKE_CXXFLAGS += -execution-charset:utf-8
msvc:QMAKE_CXXFLAGS += -source-charset:utf-8
msvc:QMAKE_CXXFLAGS_WARN_ON += -wd4819


CONFIG += c++11

SOURCES += \
        inspector.cpp \
        jscontext.cpp \
        main.cpp \
        mainwindow.cpp \
        qsslpage.cpp

HEADERS += \
        inspector.h \
        jscontext.h \
        mainwindow.h \
        qsslpage.h

FORMS += \
        inspector.ui \
        mainwindow.ui

DISTFILES += \
    msgutils.js \
    qwebchannel.js \
    testwebchannel.html

