#include "qsslpage.h"

QList<QWebEngineCertificateError::Error> QSslPage::m_allowedError =
        QList<QWebEngineCertificateError::Error>() << QWebEngineCertificateError::Error::CertificateAuthorityInvalid;

bool QSslPage::certificateError(const QWebEngineCertificateError &certError)
{
    if (m_allowedError.contains(certError.error())) {
        return true;
    } else {
        return false;
    }
}

QSslPage::QSslPage(QObject *parent) : QWebEnginePage(parent)
{

}

QSslPage::~QSslPage()
{

}

QSslPage *QSslPage::operator &()
{
    return this;
}

const QSslPage *QSslPage::operator &() const
{
    return this;
}
