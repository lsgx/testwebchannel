#ifndef INSPECTOR_H
#define INSPECTOR_H

#include <QDialog>
#include <QWebEngineView>
#include <QtWebChannel>

QT_BEGIN_NAMESPACE
namespace Ui { class Inspector; }
QT_END_NAMESPACE

class Inspector : public QDialog
{
    Q_OBJECT

private:
    QWebEngineView * m_webView = nullptr;
    QWebChannel * m_webChannel = nullptr;

public slots:
    void show();

public:
    explicit Inspector(QWidget *parent = nullptr);

    virtual ~Inspector();

    Inspector * operator &();
    Inspector const * operator &() const;

private:
    Inspector(const Inspector &) = delete;
    Inspector(Inspector &&) = delete;

    Inspector & operator =(const Inspector &) = delete;
    Inspector & operator =(Inspector &&) = delete;

private:
    Ui::Inspector * ui;
};

#endif // INSPECTOR_H
