#ifndef JSCONTEXT_H
#define JSCONTEXT_H

#include <QObject>
#include <QWebEngineView>
#include <QtWebChannel>

class JsContext : public QObject
{
    Q_OBJECT

signals:
    void recvMsg(const QString & msg);

public:
    // 向页面发送消息
    void sendMsg(QWebEnginePage * page, const QString & msg);

public slots:
    // 接收到页面发送来的消息
    void onMsg(const QString & msg);

public:
    explicit JsContext(QObject *parent = nullptr);

    virtual ~JsContext();

    JsContext * operator &();
    JsContext const * operator &() const;

private:
    JsContext(const JsContext &) = delete;
    JsContext(JsContext &&) = delete;

    JsContext & operator =(const JsContext &) = delete;
    JsContext & operator =(JsContext &&) = delete;
};

#endif // JSCONTEXT_H
