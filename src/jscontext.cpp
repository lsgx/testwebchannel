#include "jscontext.h"

void JsContext::sendMsg(QWebEnginePage *page, const QString &msg)
{
    page->runJavaScript(QString::fromUtf8("recvMessage('%1')").arg(msg));
}

void JsContext::onMsg(const QString &msg)
{
    emit recvMsg(msg);
}

JsContext::JsContext(QObject *parent) : QObject(parent)
{

}

JsContext::~JsContext()
{

}

JsContext *JsContext::operator &()
{
    return this;
}

const JsContext *JsContext::operator &() const
{
    return this;
}
