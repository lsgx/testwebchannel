#ifndef QSSLPAGE_H
#define QSSLPAGE_H

#include <QObject>
#include <QWebEngineCertificateError>
#include <QWebEnginePage>

class QSslPage : public QWebEnginePage
{
    Q_OBJECT

protected:
    virtual bool certificateError(const QWebEngineCertificateError &certError) override;

private:
    static QList<QWebEngineCertificateError::Error> m_allowedError;

public:
    explicit QSslPage(QObject *parent = nullptr);

    virtual ~QSslPage() override;

    QSslPage * operator &();
    QSslPage const * operator &() const;

private:
    QSslPage(const QSslPage &) = delete;
    QSslPage(QSslPage &&) = delete;

    QSslPage & operator =(const QSslPage &) = delete;
    QSslPage & operator =(QSslPage &&) = delete;
};

#endif // QSSLPAGE_H
