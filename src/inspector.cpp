#include "inspector.h"
#include "ui_inspector.h"

#include <QStackedLayout>

void Inspector::show()
{
    m_webView->reload();
    QDialog::show();
}

Inspector::Inspector(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Inspector)
{
    ui->setupUi(this);

    QObject::connect(ui->btnClose, &QPushButton::clicked, this, [this](){
        QDialog::close();
    });

    m_webView = new QWebEngineView(this);
    QStackedLayout * stackLayout = new QStackedLayout(ui->frame);
    ui->frame->setLayout(stackLayout);
    stackLayout->addWidget(m_webView);
    m_webView->load(QUrl::fromUserInput(QString::fromUtf8("http://localhost:7777")));
    this->show();
}

Inspector::~Inspector()
{
    if (ui)
    {
        delete ui;
        ui = nullptr;
    }
}

Inspector *Inspector::operator &()
{
    return this;
}

const Inspector *Inspector::operator &() const
{
    return this;
}
