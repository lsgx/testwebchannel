# testwebchannel

使用 QWebEngineView 和 qwebchannel 实现自定义浏览器控制

![访问网站](screen/webengine.png)
![内置DevTools](screen/InspectPage.png)
![发送消息给页面](screen/app2page.png)
![页面发送消息给QWebEngineView](screen/page2app.png)

## WebEngine的架构

Qt提供了WebEngine模块以支持Web功能。<br />
Qt WebEngine基于google的开源浏览器chromium实现，类似的项目还有cef、miniblink等等。<br />
QtWebEngine可以看作是一个完整的chromium浏览器。<br />

QtWebEngine提供了C++和Qml的接口，可以在Widget/Qml中渲染HTML、XHTML、SVG，也支持CSS样式表和JavaScript脚本。

![页面发送消息给QWebEngineView](screen/WebEngineModule.png)


基于Chromium封装了一个WebEngineCore模块，在此之上，<br />
WebEngine Widgets模块专门用于Widget项目，<br />
WebEngine 模块用于Qml项目，<br />
WebEngineProcess则是一个单独的进程，用来渲染页面、运行js脚本。<br />
Web在单独的进程里，我们开发的时候知道这一点就好了，不需要额外关注。<br />
